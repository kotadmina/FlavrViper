//
//  NetworkService.swift
//  FlavrViper
//
//  Created by Sher Locked on 02.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class NetworkService: NetworkServiceProtocol {
    
    private let serverURL = "https://flavr-3c039.firebaseio.com/"
    
    func getAllCategories(completion: @escaping ([Category]?) -> Void) {
        
        let urlString = serverURL + "categories.json"
        guard let url = URL(string: urlString) else {
            completion([])
            return
        }
        
        Alamofire.request(url, method: .get).responseJSON { response in
            guard response.result.isSuccess,
                let data = response.data,
                let json = try? JSON(data: data) else {
                completion([])
                return
            }
            let jsonArray = json.arrayValue
            let categoriesArray = jsonArray.map({ value -> Category in
                let name = value["name"].stringValue
                return Category(name: name)
            })
            completion(categoriesArray)
        }
    }
}
