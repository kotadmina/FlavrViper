//
//  ModulesAssembly.swift
//  FlavrViper
//
//  Created by Sher Locked on 28.06.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

class ModulesAssembly: BaseAssembly {
    
    static func configure() {
        HomeViewAssembly.configure()
        CategoriesAssembly.configure()
        FavouritesAssembly.configure()
        ProfileAssembly.configure()
        OpenCategoryAssembly.configure()
    }
    
}
