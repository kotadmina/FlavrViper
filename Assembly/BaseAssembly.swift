//
//  BaseAssembly.swift
//  FlavrViper
//
//  Created by Sher Locked on 28.06.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation
import Swinject

protocol BaseAssembly {
    static func configure()
}

extension BaseAssembly {
    static func defaultContainer() -> Container {
        return (UIApplication.shared.delegate as? AppDelegate)?.container ?? Container()
    }
    
    static func resolve<Service>(type: Service.Type) -> Service? {
        return defaultContainer().resolve(type)
    }
}
