//
//  OpenCategoryView.swift
//  FlavrViper
//
//  Created by Sher Locked on 03.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class OpenCategoryView: UIViewController, OpenCategoryViewInput {

    @IBOutlet weak var tableView: UITableView!
    var output: OpenCategoryViewOutput!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    func configure() {
        view.backgroundColor = ColorsLibrary.backgroundGray
    }


}
