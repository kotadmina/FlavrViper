//
//  OpenCategoryAssembly.swift
//  FlavrViper
//
//  Created by Sher Locked on 03.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
import Swinject

class OpenCategoryAssembly: BaseAssembly {
    
    static func configure() {
        let container = defaultContainer()
        
        //Register View
        container.register(OpenCategoryViewInput.self) { r  in
            let storyboard = UIStoryboard(name: "OpenCategoryView", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "OpenCategoryView") as! OpenCategoryView
            vc.output = r.resolve(OpenCategoryViewOutput.self, argument: (vc as OpenCategoryViewInput))!
            return vc
        }.inObjectScope(.transient)
        
        //Register Presenter
        container.register(OpenCategoryViewOutput.self) { (r: Resolver, view: OpenCategoryViewInput) in
            let presenter = OpenCategoryPresenter()
            return presenter
        }.inObjectScope(.transient)
        
    }

}
