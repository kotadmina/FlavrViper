//
//  Assembly.swift
//  FlavrViper
//
//  Created by Sher Locked on 02.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
import Swinject

class ProfileAssembly: BaseAssembly {
    
    static func configure() {
        let container = defaultContainer()
        
        //Register View
        container.register(ProfileViewInput.self) { r in
            let storyboard = UIStoryboard(name: "ProfileView", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
            return vc
        }.inObjectScope(.transient)
        
    }
}
