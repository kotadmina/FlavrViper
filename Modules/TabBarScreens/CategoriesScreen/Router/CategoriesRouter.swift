//
//  CategoriesRouter.swift
//  FlavrViper
//
//  Created by Sher Locked on 02.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class CategoriesRouter: CategoriesRouterInput {
    
    weak var view: UIViewController!
    
    func openCategoriesRecipeScreen(name: String) {
        let vc = ModulesAssembly.resolve(type: OpenCategoryViewInput.self) as! UIViewController
        ((vc as? OpenCategoryView)?.output as? OpenCategoryPresenter)?.categoryName = name
        view.navigationController?.pushViewController(vc, animated: true)
    }
    
}
