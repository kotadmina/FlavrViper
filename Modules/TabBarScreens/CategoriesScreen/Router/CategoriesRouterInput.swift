//
//  CategoriesRouterInput.swift
//  FlavrViper
//
//  Created by Sher Locked on 02.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
import Swinject
protocol CategoriesRouterInput {
    
    func openCategoriesRecipeScreen(name: String)
    
}
