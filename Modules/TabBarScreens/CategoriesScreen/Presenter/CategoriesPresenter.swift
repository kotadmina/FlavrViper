//
//  CategoriesPresenter.swift
//  FlavrViper
//
//  Created by Sher Locked on 02.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

class CategoriesPresenter: CategoriesViewOutput {
    
    weak var view: CategoriesViewInput!
    var interactor: CategoriesInteractorInput!
    var router: CategoriesRouterInput!
    var vmf: CategoriesVMFProtocol!
    
    func viewDidLoad() {
        let categories = interactor.getAllCategories { categories in
            let viewModels = self.vmf.createViewModel(with: categories ?? [])
            DispatchQueue.main.async {
                self.view.updateTable(with: viewModels)
            }
        }
    }
    
    func selectedCategory(with viewModel: CategoriesViewModel) {
        guard let name = viewModel.categoryName else {
            return
        }
        router.openCategoriesRecipeScreen(name: name)
    }

}
