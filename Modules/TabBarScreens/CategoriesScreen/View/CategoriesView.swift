//
//  CategoriesView.swift
//  FlavrViper
//
//  Created by Sher Locked on 02.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class CategoriesView: UIViewController, CategoriesViewInput {

    @IBOutlet weak var tableView: UITableView!
    
    var output: CategoriesViewOutput!
    
    var viewModels: [CategoriesViewModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        output.viewDidLoad()
    }
    
    func configure() {
        view.backgroundColor = ColorsLibrary.backgroundGray
        tableView.delegate = self
        tableView.dataSource = self
        let nib = UINib(nibName: "CategoriesCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "CategoriesCell")
        tableView.separatorStyle = .none
    }
    
    func updateTable(with viewModels: [CategoriesViewModel]) {
        self.viewModels = viewModels
        tableView.reloadData()
    }

}

extension CategoriesView: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CategoriesCell.cellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        output.selectedCategory(with: viewModels[indexPath.row])
    }
    
}

extension CategoriesView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoriesCell") as! CategoriesCell
        cell.configure(with: viewModels[indexPath.row])
        return cell
    }
}
