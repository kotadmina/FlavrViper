//
//  CategoriesAssembly.swift
//  FlavrViper
//
//  Created by Sher Locked on 02.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
import Swinject

class CategoriesAssembly: BaseAssembly {
    
    
    static func configure() {
        let container = defaultContainer()
        
        // Register interactor
        container.register(CategoriesInteractorInput.self) { r in
            let interactor = CategoriesInteractor()
            interactor.networkService = r.resolve(NetworkServiceProtocol.self)!
            return interactor
            }.inObjectScope(.transient)
        
        // Register view
        container.register(CategoriesViewInput.self) { r in
            let storyboard = UIStoryboard(name: "CategoriesView", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CategoriesView") as! CategoriesView
            vc.output = r.resolve(CategoriesViewOutput.self, argument: (vc as CategoriesViewInput))!
            return vc
            }.inObjectScope(.transient)
        
        // Register presenter
        container.register(CategoriesViewOutput.self) { (r: Resolver, view: CategoriesViewInput) in
            let presenter = CategoriesPresenter()
            presenter.interactor = r.resolve(CategoriesInteractorInput.self)!
            presenter.view = view
            presenter.vmf = r.resolve(CategoriesVMFProtocol.self)!
            presenter.router = r.resolve(CategoriesRouterInput.self, argument: (view as! UIViewController))!
            return presenter
            }.inObjectScope(.transient)
        
        //Register Router
        container.register(CategoriesRouterInput.self) { (r: Resolver, view: UIViewController) in
            let router = CategoriesRouter()
            router.view = view
            return router
            }.inObjectScope(.transient)
        
        //Register VMF
        container.register(CategoriesVMFProtocol.self) { _ in
            return CategoriesVMF()
        }.inObjectScope(.transient)
        
    }
    
}
