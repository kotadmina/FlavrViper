//
//  CategoriesVMF.swift
//  FlavrViper
//
//  Created by Sher Locked on 02.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

class CategoriesVMF: CategoriesVMFProtocol {
    
    func createViewModel(with categories: [Category]) -> [CategoriesViewModel] {
        return categories.map({ category -> CategoriesViewModel in
            return CategoriesViewModel(categoryName: category.name)
        })
    }
    
    
}
