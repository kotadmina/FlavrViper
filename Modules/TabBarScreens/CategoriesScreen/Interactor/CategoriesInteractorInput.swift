//
//  CategoriesInteractorInput.swift
//  FlavrViper
//
//  Created by Sher Locked on 02.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol CategoriesInteractorInput {
    func getAllCategories(completion: @escaping ([Category]?) -> Void)
}
