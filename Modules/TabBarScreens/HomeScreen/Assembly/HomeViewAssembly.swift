//
//  HomeViewAssembly.swift
//  FlavrViper
//
//  Created by Sher Locked on 28.06.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
import Swinject

class HomeViewAssembly: BaseAssembly {
    
    
    static func configure() {
        let container = defaultContainer()
        
        // Register interactor
        container.register(HomeViewInteractorInput.self) { r in
            let interactor = HomeViewInteractor()
            //interactor.eventStorage = r.resolve(EventStorageServiceProtocol.self)!
            return interactor
            }.inObjectScope(.transient)
        
        // Register view
        container.register(HomeViewInput.self) { r in
            let storyboard = UIStoryboard(name: "HomeView", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "HomeView") as! HomeView
            //vc.output = r.resolve(HomeViewOutput.self, argument: (vc as HomeViewInput))!
            return vc
            }.inObjectScope(.transient)
        
        // Register presenter
        container.register(HomeViewOutput.self) { (r: Resolver, view: HomeViewInput) in
            let presenter = HomeViewPresenter()
            //presenter.interactor = r.resolve(EventListInteractorInput.self)!
            //presenter.view = view
            //presenter.vmf = r.resolve(EventListVMFProtocol.self)!
            //presenter.router = r.resolve(EventListRouterInput.self, argument: (view as! UIViewController))!
            return presenter
            }.inObjectScope(.transient)
        
        //Register Router
        container.register(HomeViewRouterInput.self) { (r: Resolver, view: UIViewController) in
            let router = HomeViewRouter()
            //router.view = view
            return router
            }.inObjectScope(.transient)
        
    }
    
}
