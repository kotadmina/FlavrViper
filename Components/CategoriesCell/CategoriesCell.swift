//
//  CategoriesCell.swift
//  FlavrViper
//
//  Created by Sher Locked on 02.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class CategoriesCell: UITableViewCell {

    @IBOutlet weak var categoryLabel: UILabel!
    static let cellHeight: CGFloat = 64
    
    func configure(with viewModel: CategoriesViewModel) {
        categoryLabel.text = (viewModel.categoryName ?? "").uppercased()
        categoryLabel.font = FontLibrary.montserratRegular(of: 18)
    }
    
    
}
