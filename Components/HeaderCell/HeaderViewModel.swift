//
//  HeaderViewModel.swift
//  FlavrViper
//
//  Created by Sher Locked on 03.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

struct HeaderViewModel {
    
    var imageURLString: String?
    var categoryName: String?
    var amount: Int?
    
}
