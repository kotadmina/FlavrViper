//
//  HeaderView.swift
//  FlavrViper
//
//  Created by Sher Locked on 03.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class HeaderView: UIView {

    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var amountOfRecipesLabel: UILabel!
    
    func configure(with viewModel: HeaderViewModel) {
        categoryLabel.text = "category".uppercased()
        categoryLabel.font = FontLibrary.montserratRegular(of: 10)
        categoryLabel.textColor = ColorsLibrary.textGray
        
    }
    
}
