//
//  ColorsLibrary.swift
//  FlavrViper
//
//  Created by Sher Locked on 01.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

struct ColorsLibrary {
    static var primaryOrange = UIColor(red: 255/255, green: 140/255, blue: 43/255, alpha: 1)
    static var secondaryOrange = UIColor(red: 255/255, green: 99/255, blue: 34/255, alpha: 1)
    static var backgroundGray = UIColor(white: 247/255, alpha: 1)
    static var textGray = UIColor(white: 158/255, alpha: 1)
}
