//
//  MainScreenConfigurator.swift
//  FlavrViper
//
//  Created by Sher Locked on 01.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
import Swinject

class MainScreenConfigurator {
    
    static func createTabBarScreen(container: Container) -> UIViewController {
        let vcHome = container.resolve(HomeViewInput.self) as! UIViewController
        let navHome = UINavigationController(rootViewController: vcHome)
        let vcCategories = container.resolve(CategoriesViewInput.self) as! UIViewController
        let navCategories = UINavigationController(rootViewController: vcCategories)
        let vcFavourites = container.resolve(FavouritesViewInput.self) as! UIViewController
        let navFavourites = UINavigationController(rootViewController: vcFavourites)
        let vcProfile = container.resolve(ProfileViewInput.self) as! UIViewController
        let navProfile = UINavigationController(rootViewController: vcProfile)
        let controllers = [navHome, navCategories, navFavourites, navProfile]
        let tabBarVC = UITabBarController()
        tabBarVC.tabBar.tintColor = ColorsLibrary.primaryOrange
        vcHome.tabBarItem = UITabBarItem(title: "Home", image: UIImage(named: "ic_home_grey"), selectedImage: UIImage(named: "ic_home_orange"))
        vcCategories.tabBarItem = UITabBarItem(title: "Categories", image: UIImage(named: "ic_recipes_grey"), selectedImage: UIImage(named: "ic_recipes_orange"))
        vcFavourites.tabBarItem = UITabBarItem(title: "Favourites", image: UIImage(named: "ic_favorites_grey"), selectedImage: UIImage(named: "ic_favorotes_orange"))
        vcProfile.tabBarItem = UITabBarItem(title: "Profile", image: UIImage(named: "ic_profile_grey"), selectedImage: UIImage(named: "ic_profile_orange"))
        tabBarVC.viewControllers = controllers
    
        return tabBarVC
    }
}
