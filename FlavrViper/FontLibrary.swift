//
//  FontLibrary.swift
//  FlavrViper
//
//  Created by Sher Locked on 01.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

struct FontLibrary {
    static func montserratRegular(of size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-Regular", size: size)!
        return font
    }
    static func montserratBold(of size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-Bold", size: size)!
        return font
    }
    
}
